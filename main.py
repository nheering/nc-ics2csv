import os
import csv
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog

STICKY = W + N + E + S
stdd_padx = 10
stdd_pady = 10
stdd_entry_width = 30
result = {}

def get_input_file():
    filename = filedialog.askopenfilename(
        initialdir=os.getcwd(),
        title="Importdatei wählen"
    )
    etr_input_file.delete(0, END)
    etr_input_file.insert(0, filename)


def import_and_read():
    jahr = etr_input_jahr.get()
    import_file = etr_input_file.get()

    if not os.path.exists(import_file):
        messagebox.showerror("Error", "File doesn't exists.")
        return

    # Write content of file to a list
    zeilen = []
    file = open(import_file, newline='')
    for line in file:
        zeilen.append(line)

    jahr = "DTSTAMP:" + jahr
    index = 0
    for zeile in zeilen:
        # Get only the parts for the intended year
        if zeile[0:12] == jahr:
            # print("Datum: " + zeilen[index])
            i = 1
            while zeilen[index + i][0:7] != "SUMMARY":
                i = i +1

            value = zeilen[index + i ][8:len(zeilen[index + i]) -2]
            if (value in result):
                result[value] = result.get(value) + 1
            else:
                result[value] = 1

        index = index + 1
    etr_result.insert(0, result)
    print(result)
def export_data():
    with open('export.csv', 'w') as f:
        writer = csv.writer(f, delimiter=";")
        for k, v in result.items():
            writer.writerow([k, v])


if __name__ == '__main__':
    wnd_haupt = Tk()
    wnd_haupt.title("NC-ics2csv")

    lbl_input_jahr = Label(wnd_haupt, text="Year", anchor="w")
    lbl_input_jahr.grid(row=0, column=0, padx=stdd_padx, pady=(stdd_pady, 0), sticky=STICKY)
    etr_input_jahr = Entry(wnd_haupt, width=stdd_entry_width)
    etr_input_jahr.grid(row=0, column=1, padx=stdd_padx, pady=(stdd_pady, 0), sticky=STICKY)
    lbl_input_file = Label(wnd_haupt, text="Inputfile (ics)", anchor="w")
    lbl_input_file.grid(row=1, column=0, padx=stdd_padx, pady=(0, stdd_pady), sticky=STICKY)
    etr_input_file = Entry(wnd_haupt, width=stdd_entry_width)
    etr_input_file.grid(row=1, column=1, padx=stdd_padx, pady=(0, stdd_pady), sticky=STICKY)
    btn_input_file = Button(wnd_haupt, text="...", command=get_input_file)
    btn_input_file.grid(row=1, column=2, padx=stdd_padx, pady=(0, stdd_pady), sticky=STICKY)
    etr_result = Entry(wnd_haupt, width=stdd_entry_width)
    etr_result.grid(row=2, column=0, padx=stdd_padx, pady=stdd_pady, columnspan=3, sticky=STICKY)
    btn_import = Button(wnd_haupt, text="Import and Read", command=import_and_read)
    btn_import.grid(row=3, column=0, padx=stdd_padx, pady=(stdd_pady, 0), columnspan=3, sticky=STICKY)
    btn_export = Button(wnd_haupt, text="Export", command=export_data)
    btn_export.grid(row=4, column=0, padx=stdd_padx, pady=(0, 0), columnspan=3, sticky=STICKY)
    btn_exit = Button(wnd_haupt, text="Quit", command=wnd_haupt.quit)
    btn_exit.grid(row=5, column=0, padx=stdd_padx, pady=(0,stdd_pady), columnspan=3, sticky=STICKY)
    mainloop()